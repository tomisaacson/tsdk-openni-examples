cmake_minimum_required(VERSION 3.5)

set(OCLEA_PIPELINE_UTIL_INCLUDES
    ${CMAKE_CURRENT_SOURCE_DIR}/include)

set(OCLEA_PIPELINE_UTIL_DEPS
    oclea_pipeline_util
    gstrtspserver-1.0
    ${GST_DEPS})

include_directories(${GSTREAMER_INCLUDES})
include_directories(${GST_RTSP_SERVER_INCLUDES})
include_directories(${OCLEA_PIPELINE_UTIL_INCLUDES})

file(GLOB_RECURSE OCLEA_PIPELINE_UTIL_SOURCE
    "${CMAKE_CURRENT_SOURCE_DIR}/lib/oclea_gstreamer_pipeline.cpp")

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/depth_printer)
