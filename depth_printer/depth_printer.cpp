// https://github.com/OpenNI/OpenNI2/blob/master/Samples/EventBasedRead/main.cpp

#include "OpenNI.h"

#include <opencv2/opencv.hpp>

#include <iostream>
//#include <stdio.h>

using namespace std;
using namespace openni;

static bool bFinish = false;

struct BinData
{
    int minX;
    int maxX;
    int minY;
    int maxY;
    int bottom;
    int top;
};

// col, row
static const BinData s_binData[4][4] = {
    {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}},
    {{192, 301, 63, 122, 760, 960}, {197, 295, 140, 195, 760, 960}, {192, 292, 213, 266, 760, 960}, {189, 291, 292, 351, 760, 960}},
    {{325, 436, 67, 119, 760, 960}, {320, 431, 142, 200, 760, 960}, {315, 429, 217, 270, 760, 960}, {313, 434, 289, 361, 760, 960}},
    {{460, 573, 67, 124, 760, 900}, {456, 574, 141, 201, 760, 960}, {456, 570, 218, 280, 760, 960}, {456, 576, 300, 367, 760, 960}}
};


// template<typename T>
// void TsavePCD(const std::string& filename, const int width, const int height, const T *pData)
// {
// 	const int size = width * height;

//     FILE* fp = fopen(filename.c_str(), "wt");
//     fprintf(fp, "VERSION .7\n");
//     fprintf(fp, "FIELDS x y z\n");
//     fprintf(fp, "SIZE 4 4 4\n");
//     fprintf(fp, "TYPE F F F\n");
//     fprintf(fp, "COUNT 1 1 1\n");
//     fprintf(fp, "WIDTH %d\n", width);
//     fprintf(fp, "HEIGHT %d\n", height);
//     fprintf(fp, "VIEWPOINT 0 0 0 1 0 0 0\n");
//     fprintf(fp, "POINTS %d\n", size);
//     fprintf(fp, "DATA ascii\n");

//     for (int y = 0; y < height; y++)
//     {
//         for (int x = 0; x < width; x++)
//         {
//             size_t index = (width * y) + x;
//             T point = pData[index];
//             if (point == 0)
//             {
//                fprintf(fp, "%d %d nan\n", x, y);
//             }
//             else
//             {
// 		       if (std::is_same<T, float>::value)
// 			   {
// 	               fprintf(fp, "%d %d %f\n", x, y, point);
// 			   }
// 			   else
// 			   {
// 	               fprintf(fp, "%d %d %d\n", x, y, point);
// 			   }
//             }
//         }
//     }
//     fclose(fp);
// }


class FrameCallback : public VideoStream::NewFrameListener
{
public:
	FrameCallback() : m_frameReceived(false) {}

	void onNewFrame(VideoStream& stream) override
	{
		stream.readFrame(&m_frame);

		const int frameIndex = m_frame.getFrameIndex();
		DepthPixel* pDepth = (DepthPixel *)m_frame.getData();
		cv::Mat src(m_frame.getHeight(), m_frame.getWidth(), CV_16U, pDepth);

		if (!m_frameReceived)
		{
			m_firstFrame = frameIndex;
			m_frameReceived = true;
		}
		else if (frameIndex - m_firstFrame == 30)
		{
			savePCD("frame.pcd", m_frame.getWidth(), m_frame.getHeight(), static_cast<const uint16_t *>(m_frame.getData()));
			// bFinish = true;
		}

		update_percentages(src);
	}

private:
	void analyzeFrame(const VideoFrameRef& frame);
	void savePCD(const std::string& filename, const int width, const int height, const uint16_t *pData);
	void savePCD(const std::string& filename, const cv::Mat& frame);

	void update_percentage(cv::Mat &src, const int col, const int row, const int minX, const int maxX, const int minY, const int maxY, const int bottom, const int top);
	void update_percentages(cv::Mat &src);

	VideoFrameRef m_frame;
	bool m_frameReceived;
	int m_firstFrame;
};


void FrameCallback::update_percentage(cv::Mat &src, const int col, const int row, const int minX, const int maxX, const int minY, const int maxY, const int bottom, const int top)
{
	short min = 10000;
	short max = 0;
	float total = 0;
	int valid = 0;
	int count = 0;
	for (int x = minX; x < maxX; x++)
	{
		for (int y = minY; y < maxY; y++)
		{
			count++;
			const uint16_t z = src.at<uint16_t>(y, x);
			// cout << z << ", ";
			if (z > 600) // distance from camera to freezer lid to cut out noise
			{
				valid++;
				total += z;
				if (z < min) min = z;
				if (z > max) max = z;
			}
		}            
	}
	float avg = total / valid;
	float floor = top - avg;
	float percent = 0;
	if (floor > 0) percent = (floor / (top - bottom)) * 100;
	// percentages_[col][row] = percent;

	std::string name = "r" + std::to_string(row) + "c" + std::to_string(col);
	printf("%s: Pct: %2.2f, Avg: %2.2f, Min: %d, Max: %d, Valid: %d / %d\n", name.c_str(), percent, avg, min, max, valid, count);
}


void FrameCallback::update_percentages(cv::Mat &src)
{
	for (int col = 0; col < 4; col++)
	{
		for (int row = 0; row < 4; row++)
		{
			if (s_binData[col][row].minX == 0)
				continue;

			update_percentage(src, col, row,
				s_binData[col][row].minX, s_binData[col][row].maxX,
				s_binData[col][row].minY, s_binData[col][row].maxY,
				s_binData[col][row].bottom, s_binData[col][row].top);
		}
	}
}


void FrameCallback::savePCD(const std::string& filename, const int width, const int height, const uint16_t *pData)
{
	const int size = width * height;

    FILE* fp = fopen(filename.c_str(), "wt");
    fprintf(fp, "VERSION .7\n");
    fprintf(fp, "FIELDS x y z\n");
    fprintf(fp, "SIZE 4 4 4\n");
    fprintf(fp, "TYPE F F F\n");
    fprintf(fp, "COUNT 1 1 1\n");
    fprintf(fp, "WIDTH %d\n", width);
    fprintf(fp, "HEIGHT %d\n", height);
    fprintf(fp, "VIEWPOINT 0 0 0 1 0 0 0\n");
    fprintf(fp, "POINTS %d\n", size);
    fprintf(fp, "DATA ascii\n");

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            size_t index = (width * y) + x;
            uint16_t point = pData[index];
            if (point == 0)
            {
                fprintf(fp, "%d %d nan\n", x, y);
            }
            else
            {
               fprintf(fp, "%d %d %d\n", x, y, point);
            }
        }
    }
    fclose(fp);
}


void FrameCallback::savePCD(const std::string& filename, const cv::Mat& frame)
{
	const int width = frame.cols;
	const int height = frame.rows;
	const int size = width * height;

    FILE* fp = fopen(filename.c_str(), "wt");
    fprintf(fp, "VERSION .7\n");
    fprintf(fp, "FIELDS x y z\n");
    fprintf(fp, "SIZE 4 4 4\n");
    fprintf(fp, "TYPE F F F\n");
    fprintf(fp, "COUNT 1 1 1\n");
    fprintf(fp, "WIDTH %d\n", width);
    fprintf(fp, "HEIGHT %d\n", height);
    fprintf(fp, "VIEWPOINT 0 0 0 1 0 0 0\n");
    fprintf(fp, "POINTS %d\n", size);
    fprintf(fp, "DATA ascii\n");

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            float point = frame.at<float>(y, x);
            if (point == 0)
            {
                fprintf(fp, "%d %d nan\n", x, y);
            }
            else
            {
               fprintf(fp, "%d %d %f\n", x, y, point);
            }
        }
    }
    fclose(fp);
}


void FrameCallback::analyzeFrame(const VideoFrameRef& frame)
{
	// int test = (frame.getWidth() * 200) + 320;
	int middleIndex = (frame.getHeight() + 1) * frame.getWidth() / 2;

	switch (frame.getVideoMode().getPixelFormat())
	{
		case PIXEL_FORMAT_DEPTH_1_MM:
		case PIXEL_FORMAT_DEPTH_100_UM:
		{
			DepthPixel* pDepth = (DepthPixel *)frame.getData();
			printf("[%08llu] %d %8d\n",
				(long long)frame.getTimestamp(),
				frame.getFrameIndex(),
				pDepth[middleIndex]);
		}
		break;
		case PIXEL_FORMAT_RGB888:
		{
			RGB888Pixel* pColor = (RGB888Pixel *)frame.getData();
			printf("[%08llu] %d 0x%02x%02x%02x\n",
				(long long)frame.getTimestamp(),
				frame.getFrameIndex(),
				pColor[middleIndex].r & 0xff,
				pColor[middleIndex].g & 0xff,
				pColor[middleIndex].b & 0xff);
		}
		break;
		default:
			printf("Unknown format\n");
			break;
	}
}


class OpenNIDeviceListener : public OpenNI::DeviceConnectedListener,
									public OpenNI::DeviceDisconnectedListener,
									public OpenNI::DeviceStateChangedListener
{
public:
	virtual void onDeviceStateChanged(const DeviceInfo* pInfo, DeviceState state) 
	{
		printf("Device \"%s\" error state changed to %d\n", pInfo->getUri(), state);
	}

	virtual void onDeviceConnected(const DeviceInfo* pInfo)
	{
		printf("Device \"%s\" connected\n", pInfo->getUri());
	}

	virtual void onDeviceDisconnected(const DeviceInfo* pInfo)
	{
		printf("Device \"%s\" disconnected\n", pInfo->getUri());
	}
};


int main(int argc, char** argv)
{
	// rectangles = [(cv2.minAreaRect(cnt)) for cnt in contours]  
	// for rect in rectangles:
	// 	rect = cv2.boxPoints(rect)
	// 	rect = np.int0(rect)
	// 	coords = cv2.boundingRect(rect)
	// 	rect[:,0] = rect[:,0] - coords[0]
	// 	rect[:,1] = rect[:,1] - coords[1]
	// 	area = cv2.contourArea(rect)
	// 	zeros = np.zeros((coords[3], coords[2]), np.uint8)
	// 	cv2.fillConvexPoly(zeros, rect, 255)
	// 	im = greyscale[coords[1]:coords[1]+coords[3], 
	// 	coords[0]:coords[0]+coords[2]]
	// 	print(np.sum(cv2.bitwise_and(zeros,im))/255)

	// vector<cv::Point2f> v{cv::Point2f(279, 117), cv::Point2f(369, 118), cv::Point2f(284, 175), cv::Point2f(369, 170)};
	// cv::InputArray points(v);
	// cv::RotatedRect rotatedRect = cv::minAreaRect(points);
	// cv::Point2f outputPoints[4];
	// rotatedRect.points(outputPoints);
	// coords = cv::boundingRect(outputPoints);

	// cv::fillConvexPoly()
	
    const char* deviceURI = openni::ANY_DEVICE;
    if (argc > 1)
    {
        deviceURI = argv[1];
    }

    Version version = OpenNI::getVersion();
    cout << "OpenNI::getVersion() = " << version.major << "." << version.minor << "." << version.maintenance << "." << version.build << endl;

    Status result = STATUS_OK;

    // 0 - Verbose; 1 - Info; 2 - Warning; 3 - Error. Default - None
    result = OpenNI::setLogMinSeverity(3);
    cout << "OpenNI::setLogMinSeverity(3) = " << result << endl;

    result = OpenNI::setLogConsoleOutput(true);
    cout << "OpenNI::setLogConsoleOutput() = " << result << endl;

    // result = OpenNI::setLogOutputFolder("/mnt/media/");
    // cout << "OpenNI::setLogOutputFolder() = " << result << endl;

    // result = OpenNI::setLogFileOutput(true);
    // cout << "OpenNI::setLogFileOutput() = " << result << endl;

    // char strFileName[1024] = "";
    // result = OpenNI::getLogFileName(strFileName, 1024);
    // cout << "OpenNI::getLogFileName() = " << strFileName << endl;

    result = OpenNI::initialize();
    cout << "OpenNI::initialize() = " << result << endl;

	OpenNIDeviceListener devicePrinter;

	OpenNI::addDeviceConnectedListener(&devicePrinter);
	OpenNI::addDeviceDisconnectedListener(&devicePrinter);
	OpenNI::addDeviceStateChangedListener(&devicePrinter);

    openni::Array<openni::DeviceInfo> deviceList;
	openni::OpenNI::enumerateDevices(&deviceList);
    cout << "OpenNI::enumerateDevices() = " << deviceList.getSize() << endl;
	for (int i = 0; i < deviceList.getSize(); ++i)
	{
		cout << "Device " << deviceList[i].getUri() << " already connected" << endl;
	}

    Device device;
    VideoStream depthStream, colorStream;
    result = device.open(deviceURI);
    cout << "device.open() = " << result << endl;
    if (result == STATUS_OK)
    {
        cout << "device.isValid() = " << device.isValid() << endl;
		// Status getProperty(int propertyId, void* data, int* dataSize) const
		// int getGain() {
		// bool isImageRegistrationModeSupported(ImageRegistrationMode mode) const
		// ImageRegistrationMode getImageRegistrationMode() const
		// bool getDepthColorSyncEnabled()

		// https://github.com/leezl/OpenNi-Python/blob/4a0731a1f76011260b0c044922e8d20ccb61adb6/primesense/_openni2.py
		// ONI_DEVICE_PROPERTY_FIRMWARE_VERSION = 0
		// ONI_DEVICE_PROPERTY_DRIVER_VERSION = 1
		// ONI_DEVICE_PROPERTY_HARDWARE_VERSION = 2
		// ONI_DEVICE_PROPERTY_SERIAL_NUMBER = 3
		// ONI_DEVICE_PROPERTY_ERROR_STATE = 4
		// ONI_DEVICE_PROPERTY_IMAGE_REGISTRATION = 5
		// ONI_DEVICE_PROPERTY_PLAYBACK_SPEED = 100
		// ONI_DEVICE_PROPERTY_PLAYBACK_REPEAT_ENABLED = 101
		// for (int prop = 0; prop < 10; prop++)
		// {
		// 	cout << "Prop " << prop << ": " << device.isPropertySupported(prop) << endl;
		// }
		// for (int cmd = 0; cmd < 10; cmd++)
		// {
		// 	cout << "Cmd " << cmd << ": " << device.isCommandSupported(cmd) << endl;
		// }

		// if (device.isImageRegistrationModeSupported(IMAGE_REGISTRATION_DEPTH_TO_COLOR))
		// {
		// 	device.setImageRegistrationMode(IMAGE_REGISTRATION_DEPTH_TO_COLOR);
		// }

		const SensorInfo* pSensorInfo = device.getSensorInfo(SENSOR_DEPTH);
       	if (pSensorInfo != nullptr)
	    {
			const SensorType sensorType = pSensorInfo->getSensorType();
			switch (sensorType)
			{
				case SENSOR_IR: cout << "pSensorInfo->getSensorType() = SENSOR_IR" << endl; break;
				case SENSOR_COLOR: cout << "pSensorInfo->getSensorType() = SENSOR_COLOR" << endl; break;
				case SENSOR_DEPTH: cout << "pSensorInfo->getSensorType() = SENSOR_DEPTH" << endl; break;
				default: cout << "pSensorInfo->getSensorType() = " << sensorType << endl; break;
			}

			const Array<VideoMode>& videoModes = pSensorInfo->getSupportedVideoModes();
			for (int videoMode = 0; videoMode < videoModes.getSize(); videoMode++)
			{
				cout << videoMode << ": " << videoModes[videoMode].getResolutionX() << ", " << videoModes[videoMode]. getResolutionY() << " - " << videoModes[videoMode].getFps() << ", ";
				const PixelFormat pixelFormat = videoModes[videoMode].getPixelFormat();
				switch (pixelFormat)
				{
					case PIXEL_FORMAT_DEPTH_1_MM: cout << "PIXEL_FORMAT_DEPTH_1_MM" << endl; break;
					case PIXEL_FORMAT_DEPTH_100_UM: cout << "PIXEL_FORMAT_DEPTH_100_UM" << endl; break;
					case PIXEL_FORMAT_SHIFT_9_2: cout << "PIXEL_FORMAT_SHIFT_9_2" << endl; break;
					case PIXEL_FORMAT_SHIFT_9_3: cout << "PIXEL_FORMAT_SHIFT_9_3" << endl; break;
					case PIXEL_FORMAT_RGB888: cout << "PIXEL_FORMAT_RGB888" << endl; break;
					case PIXEL_FORMAT_YUV422: cout << "PIXEL_FORMAT_YUV422" << endl; break;
					case PIXEL_FORMAT_GRAY8: cout << "PIXEL_FORMAT_GRAY8" << endl; break;
					case PIXEL_FORMAT_GRAY16: cout << "PIXEL_FORMAT_GRAY16" << endl; break;
					case PIXEL_FORMAT_JPEG: cout << "PIXEL_FORMAT_JPEG" << endl; break;
					case PIXEL_FORMAT_YUYV: cout << "PIXEL_FORMAT_YUYV" << endl; break;
					default: cout << pixelFormat << endl; break;
				}
			}

            result = depthStream.create(device, openni::SENSOR_DEPTH);
            cout << "depthStream.create() = " << result << endl;
			if (result == STATUS_OK)
			{
				if (depthStream.getMirroringEnabled())
				{
					result = depthStream.setMirroringEnabled(false);
    		        cout << "depthStream.setMirroringEnabled(false) = " << result << endl;
				}
	            result = depthStream.start();
    	        cout << "depthStream.start() = " << result << endl;
			}

			// ONI_STREAM_PROPERTY_CROPPING = 0
			// ONI_STREAM_PROPERTY_HORIZONTAL_FOV = 1
			// ONI_STREAM_PROPERTY_VERTICAL_FOV = 2
			// ONI_STREAM_PROPERTY_VIDEO_MODE = 3
			// ONI_STREAM_PROPERTY_MAX_VALUE = 4
			// ONI_STREAM_PROPERTY_MIN_VALUE = 5
			// ONI_STREAM_PROPERTY_STRIDE = 6
			// ONI_STREAM_PROPERTY_MIRRORING = 7
			// ONI_STREAM_PROPERTY_NUMBER_OF_FRAMES = 8
			// ONI_STREAM_PROPERTY_AUTO_WHITE_BALANCE = 100
			// ONI_STREAM_PROPERTY_AUTO_EXPOSURE = 101
			// ONI_STREAM_PROPERTY_EXPOSURE = 102
			// ONI_STREAM_PROPERTY_GAIN = 103
        }
       	if (device.hasSensor(SENSOR_COLOR))
	    {
            result = colorStream.create(device, openni::SENSOR_COLOR);
            cout << "colorStream.create() = " << result << endl;
			if (result == STATUS_OK)
			{
				result = colorStream.start();
				cout << "colorStream.start() = " << result << endl;
			}
        }
    }

	FrameCallback depthPrinter;

	// Register to new frame
	depthStream.addNewFrameListener(&depthPrinter);

	// Wait while we're getting frames through the printer
	while (!bFinish)
	{
		sleep(1);
	}

	depthStream.removeNewFrameListener(&depthPrinter);

    depthStream.stop();
    depthStream.destroy();
    colorStream.stop();
    colorStream.destroy();

    device.close();

    OpenNI::shutdown();
    return 0;
}
