cmake_minimum_required(VERSION 3.5)

###############################################################################
#### INCLUDES #################################################################
###############################################################################

# include_directories(${GSTREAMER_INCLUDES})
# include_directories(${BREAKPAD_INCLUDES})
# include_directories(${EDITLINE_INCLUDES})
# include_directories(${GST_RTSP_SERVER_INCLUDES})
# include_directories(${OCLEA_PIPELINE_UTIL_INCLUDES})
# include_directories(${WEBRTC_GST_INCLUDES})
include_directories(${OPENCV_INCLUDES})

include_directories(../OpenNI-Linux-Arm64-2.3.0.63/Include)

###############################################################################
#### DEPENDENCIES #############################################################
###############################################################################

link_directories(../OpenNI-Linux-Arm64-2.3.0.63/Redist)

###############################################################################
#### SOURCES ##################################################################
###############################################################################

# OpenNI
add_executable(depth_printer ${CMAKE_CURRENT_SOURCE_DIR}/depth_printer.cpp)
target_link_libraries(depth_printer ${GST_DEPS} ${GLIB_DEPS} ${OPENCV_DEPS} libOpenNI2.so)
install(TARGETS
        depth_printer
        RUNTIME DESTINATION usr/bin) 
